import Head from "next/head";
import SnowflakeApp from '../components/SnowflakeApp'

export default () => <div>
  <Head>
      <meta name="viewport" content="width=device-width, initial-scale=1" />
      <link href="https://fonts.googleapis.com/css?family=Catamaran:400,600&display=swap" rel="stylesheet"></link>
  </Head>
  <SnowflakeApp />
</div>
